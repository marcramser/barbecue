import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private http : HttpClient) { }

  //
  /**
 * Does not work because of CORS Problem (Onedrive does not allow it).
 * TODO: Request .json with official Onedrive API (https://docs.microsoft.com/en-us/onedrive/developer/rest-api/concepts/using-sharing-links#encoding-sharing-urls)
 */
  getDataByHTTP() {
    return this.http.get('https://onedrive.live.com/?authkey=%21AHENHwlTa-41ssY&cid=4BD2064073CAE964&id=4BD2064073CAE964%21406236&parId=4BD2064073CAE964%215174&o=OneUp').subscribe();
  }

  /**
  * Workaround (for this.getDataByHTTP())
  */
  getData() {
    return {
      "grill": {
        "width": 20,
        "height": 15,
        "grillitems": {
          "grillitem": [
            {
              "width": 5,
              "height": 4,
              "count": 6,
              "title": "Steak"
            },
            {
              "width": 8,
              "height": 2,
              "count": 5,
              "title": "Sausage"
            },
            {
              "width": 3,
              "height": 3,
              "count": 4,
              "title": "Tomato"
            },
            {
              "width": 4,
              "height": 3,
              "count": 8,
              "title": "Veal"
            }
          ]
        }
      }
    }
  }
}
