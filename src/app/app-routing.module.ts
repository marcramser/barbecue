import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DataService } from './data.service';

const routes: Routes = [];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [DataService]
})
export class AppRoutingModule { }
