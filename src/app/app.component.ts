import { Component } from '@angular/core';
import { DataService } from './data.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  mapOfGrill = [];
  unplaceableGrillItems = [];

  constructor(private dataService:DataService) {
  }

  ngOnInit() {
    let self = this;
    //Load data from API
    let data = self.dataService.getData();
    /**
    * Sort list by needed space of figure.
    * So this algoritmus places the big items first
    * Because usually small stuff needs less time to be done so its not a big
    * problem if you place it after everything else.
    * Plus I basically prefer meat over tomatoes... :)
    *
    * NOTE: Would be better to use a VLSI
    * http://www.collectionscanada.gc.ca/obj/s4/f2/dsk1/tape3/PQDD_0028/MQ52380.pdf
    */
    data.grill.grillitems.grillitem.sort(function(item_a, item_b) {
        return (item_b.height * item_b.width) - (item_a.height * item_a.width);
    });

    //Create 2D array which  represents the places on the grill
    self.mapOfGrill = new Array(data.grill.height);
    for (let i = 0; i < self.mapOfGrill.length; i++) {
      self.mapOfGrill[i] = new Array(data.grill.width);
    }

    //Creates an instance for each grillitem
    var effectivGrillItems = [];
    data.grill.grillitems.grillitem.forEach(function(item) {
      for (let count = 0; count < item.count; count++) {
        effectivGrillItems.push(JSON.parse(JSON.stringify(item))); //Deep copy
      }
    });

    //Places the grillitems on the grill
    for (let z = 0; z < effectivGrillItems.length; z++) {
       let addToMapofGrill = function(item) {
         for (let i = 0; i < self.mapOfGrill.length; i++) {
           for (let j = 0; j < self.mapOfGrill[i].length; j++) {
             //If place is free and enough place for whole grill item
             if(self.mapOfGrill[i][j] == undefined && self.mapOfGrill.length - i >= item.height && self.mapOfGrill[i].length - j >= item.width) {
               for (let g = 0; g < item.height; g++) {
                 for (let k = 0; k < item.width; k++) {
                   item.top = g == 0;
                   item.left = k == 0;
                   item.bottom = g == item.height - 1;
                   item.right =  k == item.width - 1;
                   self.mapOfGrill[i + g][j + k] = JSON.parse(JSON.stringify(item)); //Deep copy;
                 }
               }
               return true;
             }
           }
         }
       }
       //Try to add grillItem to the grill. If it fails it will be added to the list of unplaceableGrillItems.
      let couldAdd = addToMapofGrill(effectivGrillItems[z]);
      if (!couldAdd) {
        self.unplaceableGrillItems.push(effectivGrillItems[z]);
      }
    }
  }
}
